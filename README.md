# Castle-Mania

## Доступные скрипты

### `npm start`

Запуск билд приложения в режиме разработки на localhost:3000, поддерживает hot-reload.

### `npm test`

Запуск unit-тестов.

### `npm run lint`

Запуск линтера.

### `npm run lint:fix`

Запуск линтера с автофиксами.

### `npm run build`

Создание минифицированного билда для продакшена.

## Дополнительная информация

Использован свой Gitlab-Runner.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
