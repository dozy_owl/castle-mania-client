import '@testing-library/jest-dom';
import {screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {renderApp, renderFragment} from 'shared/tests/render';

import {MainBanner} from 'widgets/banner/main_banner';


describe('<CourseCard>', () => {
  it('Рендерится на главной странице', async () => {
    renderApp();

    expect(screen.getByRole('heading', {level: 1})).toBeInTheDocument();
  });

  it('Содержит заголовок первого уровня, описание баннера и кнопку', async () => {
    renderFragment(<MainBanner />);

    expect(screen.getByRole('heading', {level: 1})).toBeInTheDocument();
    expect(screen.getByRole('button', {name: /узнать подробнее/i})).toBeInTheDocument();
  });

  it('Кнопка баннера интерактивна', async () => {
    renderFragment(<MainBanner />);

    userEvent.click(screen.getByRole('button', {name: /узнать подробнее/i}));
  });
});
