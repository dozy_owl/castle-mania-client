import {UIMainBanner} from 'entities/banner/main_banner';
import {UserEnterLanding} from 'features/banner/main_banner';


export const MainBanner = () => {
  return (
    <UIMainBanner>
      <UIMainBanner.Footer>
        <UIMainBanner.Emblem />
        <UserEnterLanding />
      </UIMainBanner.Footer>
    </UIMainBanner>
  );
};
