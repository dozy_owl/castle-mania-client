import {v4 as uuid} from 'uuid';

import {UICourseMap} from 'entities/course/course_map';
import {UserEnterChapter, UserNavigateMap} from 'features/course/course_info';
import {useCourse} from 'shared/lib/store';
import styles from './index.module.scss';


export const CourseMap = () => {
  const {chapters, weekProgress} = useCourse();

  return (
    <>
      <UserNavigateMap />
      <UICourseMap.Title />
      <UICourseMap>
        {chapters.map((chapter, index) => {
          const cycled = index >= 7 && index + 1 !== chapters.length;
          return (
            <UICourseMap.Tile key={uuid()} 
                              order={index}   
                              cycled={cycled}>
              <UICourseMap.Chapter order={index}
                                   cycled={cycled}
                                  progress={weekProgress[index]?.capacity || 0}
                                   {...chapter}/></UICourseMap.Tile>
          );
        })}
      </UICourseMap>
      <UserEnterChapter styled={styles.enter} />
    </>
  );
};
