import {Navigate} from 'react-router-dom';

import {UICourseInfo} from 'entities/course/course_info';
import {useCourse} from 'shared/lib/store';


export const CourseInfo = () => {
  const {name, chapters, experience} = useCourse();

  if (!name || !chapters || !experience) {
    return <Navigate to="/" />;
  }

  return (
    <UICourseInfo title={name} score={experience}>
      <UICourseInfo.Chapters chapters={chapters} />
    </UICourseInfo>
  );
};
