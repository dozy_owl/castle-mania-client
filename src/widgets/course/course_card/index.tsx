import {memo} from 'react';
import {UICourseCard} from 'entities/course/course_card';
import {useCourse} from 'shared/lib/store';


export const CourseCard = memo(() => {
  const course = useCourse();

  if (!course) {
    return null;
  }

  return (
    <UICourseCard>
      <UICourseCard.Description {...course} />
      <UICourseCard.Progress />
    </UICourseCard>
  );
});
