import {FC} from 'react';
import {Link, useParams} from 'react-router-dom';

import {CommonStyledProps} from 'shared/config/types';
import {useCourse} from 'shared/lib/store';
import {Button} from 'shared/ui';


export const UserEnterChapter: FC<CommonStyledProps> = ({styled}) => {
  const activeChapter = useCourse((state) => state.activeChapter);
  const {courseId} = useParams();

  return (
    <Link to={`/course/${courseId}/material/${activeChapter?.id}`}>
      <Button type="button" 
              styled={styled}>Приступить!</Button>
    </Link>
  );
};
