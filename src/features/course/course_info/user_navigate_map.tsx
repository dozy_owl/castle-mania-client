import classnames from 'classnames';

import {useCourse, useCourseWeek} from 'shared/lib/store';
import {Button} from 'shared/ui';
import styles from './index.module.scss';


export const UserNavigateMap = () => {
  const {chapters, activeChapter, setActiveChapter} = useCourse();
  const setWeek = useCourseWeek((state) => state.setWeek);

  const handleNextChapter = () => {
    const index = chapters.findIndex(
      (chapter) => chapter.id === activeChapter?.id);
    if (index !== -1) {
      const newActiveChapter = 
          chapters[(index + 1) % 2];
      setActiveChapter(newActiveChapter.id);
      setWeek(newActiveChapter);
    }
  };

  return (
    <div className={styles.base}>
      <Button type="button"
              styled={classnames(styles.button, styles.button__left)}
              onClick={handleNextChapter}/>
      <span>{activeChapter!.title}</span>
      <Button type="button"
              styled={classnames(styles.button, styles.button__right)}
              onClick={handleNextChapter}/>
    </div>
  );
};
