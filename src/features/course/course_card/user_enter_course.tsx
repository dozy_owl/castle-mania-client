import {useNavigate} from 'react-router-dom';
import {Button} from 'shared/ui';

import styles from '../index.module.scss';


export const UserEnterCourse = () => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/course/1');
  };

  return (
    <Button type="button" onClick={handleClick} styled={styles.base}>
      Приступить
    </Button>
  );
};
