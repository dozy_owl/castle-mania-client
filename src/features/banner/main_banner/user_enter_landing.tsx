import {Button} from 'shared/ui';


export const UserEnterLanding = () => {
  const handleClick = () => {
    window.scrollBy({top: 1000});
  };

  return (
    <Button type="button" onClick={handleClick}>
      Поехали!
    </Button>
  );
};
