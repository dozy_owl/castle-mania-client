import {useGetCourse} from 'entities/course/course_material';
import {CourseMap} from 'widgets/course';
import {Layout} from 'widgets/layout';
import styles from './index.module.scss';


const CoursePage = () => {
  const {data, isLoading, isFetching} = useGetCourse('1');

  if (!data || isLoading || isFetching) {
    return <>Loading...</>;
  }

  return (
    <Layout.Main styled={styles.header_styled} noFooter>
      <div className={styles.main_styled}>
        <CourseMap />
      </div>
    </Layout.Main>
  );
};

export default CoursePage;
