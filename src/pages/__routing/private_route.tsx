import {Navigate, Outlet} from 'react-router-dom';
import {tokenService} from 'shared/lib/token_service';


export const PrivateRoute = () => {
  if (tokenService.validateSession()) {
    return <Outlet />;
  }
  return <Navigate to="/login" />;
};
