import {RegisterPage} from 'pages/register';


export const RegistrationRoute = {
  path: '/register',
  name: 'Register',
  element: <RegisterPage />,
};
