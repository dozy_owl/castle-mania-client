import {LoginPage} from 'pages/login';


export const LoginRoute = {
  path: '/login',
  name: 'Login',
  element: <LoginPage />,
};
