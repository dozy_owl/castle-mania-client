import {NotFoundPage} from 'pages/not_found';


export const NotFoundRoute = {
  path: '*',
  name: 'Not Found',
  element: <NotFoundPage />,
};
