import {HomePage} from 'pages/home';


export const HomeRoute = {
  path: '/',
  name: 'Home',
  element: <HomePage />,
};
