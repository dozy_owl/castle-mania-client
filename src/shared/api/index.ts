import axios, {AxiosError, AxiosRequestConfig} from 'axios';

import {PostAnswerParams, PostInfo, SignInResponse, SignInParams} from 'shared/config/types';
import {tokenService} from 'shared/lib/token_service';


export const API_URL = '';

export const config: AxiosRequestConfig = {
  withCredentials: true,
  headers: {
    Authorization: `Bearer ${tokenService.getSessionToken()}`,
  },
};

export const axiosClient = axios.create({baseURL: API_URL, ...config});

axiosClient.interceptors.request.use((req) => {
  req.headers.Authorization = `Bearer ${tokenService.getSessionToken()}`;
  return req;
});

export const signIn = async (params: SignInParams) => {
  try {
    const {data} = await axiosClient.post<SignInResponse>(`/api/auth/signin`, params);
    tokenService.createSession(data.accessToken.trim());
  } catch (e) {
    console.error((e as AxiosError).message);
  }
};

export const signUp = async (params: SignInParams) => {
  try {
    await axiosClient.post(`/api/auth/signup`, params);
  } catch (e) {
    console.error((e as AxiosError).message);
  }
};

export const getCourse = async (courseId: string) => {
  try {
    const course = await axiosClient.get(`/course/${courseId}`);
    const weekProgress = await axiosClient.get(`/course/week-progress/${courseId}`);
    return {...course.data, weekProgress: weekProgress.data};
  } catch (e: any) {
    console.error((e as AxiosError).message);
  }
};

export const getCourseChapter = async (course: string, chapter: string) => {
  try {
    const {data} = await axiosClient.get(`/course/${course}/chapter/${chapter}`);
    return data;
  } catch (e: any) {
    console.error((e as AxiosError).message);
  }
};

export const getCourseWeekProgress = async (course: string) => {
  try {
    const {data} = await axiosClient.get(`/course/week-progress/${course}`);
    return data;
  } catch (e: any) {
    console.error((e as AxiosError).message);
  }
};

export const answerQuestion = async (params: PostAnswerParams) => {
  try {
    const {data} = await axiosClient.post(`/course/question`, params);
    return data;
  } catch (e: any) {
    console.error((e as AxiosError).message);
  }
};

export const postInfo = async (params: PostInfo) => {
  try {
    const {data} = await axiosClient.get(`/course/info/${params.infoId}`);
    return data;
  } catch (e: any) {
    console.error((e as AxiosError).message);
  }
};

export const apiService = {
  signIn,
  signUp,
  getCourse,
  getCourseChapter,
  getCourseWeekProgress,
  answerQuestion,
};
