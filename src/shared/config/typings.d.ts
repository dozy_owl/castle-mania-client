declare module '*.module.scss';

declare module '*.gif';
declare module '*.jpg';
declare module '*.svg';

declare namespace NodeJS {
  interface ProcessEnv {
    REACT_APP_API_URL: string;
  }
}

declare namespace React {
  function lazy<T extends ComponentType<any>>(factory: () => Promise<{default: T}>): T;
}
