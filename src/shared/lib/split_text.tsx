import {v4 as uuid} from 'uuid';


export const splitText = (text: string, styled: string) => {
  return (
    <>
      {text.split('').map((char) => (
        <span key={uuid()} className={styled}>
          {char}
        </span>
      ))}
    </>
  );
};
