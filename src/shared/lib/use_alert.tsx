import {useCallback, useState} from 'react';


const delay = (ms: number) => new Promise((res) => setTimeout(res, ms));

export const useAlert = () => {
  const [hovering, setHovering] = useState(false);

  const handleMouseEnter = useCallback(async () => {
    await delay(100);
    setHovering(true);
  }, []);

  const handleMouseLeave = useCallback(async () => {
    await delay(99);
    setHovering(false);
  }, []);

  return {hovering, handleMouseEnter, handleMouseLeave};
};
