import {useCallback, useState} from 'react';


export const useAccordion = () => {
  const [activeItem, setActiveItem] = useState(1);

  const handleClick = useCallback((index: number) => setActiveItem(index), []);

  return {activeItem, handleClick};
};
