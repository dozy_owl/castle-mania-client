import classnames from 'classnames';
import {FC, ReactNode} from 'react';

import {CommonStyledProps} from 'shared/config/types';
import {Template} from 'shared/ui';
import styles from './index.module.scss';
import {AccordionItem} from './specs';


type AccordionExtensions = {
  Item: typeof AccordionItem;
};

type AccordionProps = CommonStyledProps & {children: ReactNode};

export const Accordion: FC<AccordionProps> & AccordionExtensions = ({
  styled,
  children,
}) => {
  return (
    <Template.FlexWrapper styled={classnames(styles.base, styled && styled)}>
      {children}
    </Template.FlexWrapper>
  );
};

Accordion.Item = AccordionItem;
