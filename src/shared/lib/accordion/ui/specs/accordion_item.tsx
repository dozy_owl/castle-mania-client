import classnames from 'classnames';
import {FC, ReactNode, useRef, useState} from 'react';

import {CommonStyledProps} from 'shared/config/types';
import {Button} from 'shared/ui';
import styles from '../index.module.scss';


type AccordionItemProps = CommonStyledProps & {
  title: ReactNode;
  children: ReactNode;
};

export const AccordionItem: FC<AccordionItemProps> = ({styled, title, children}) => {
  const [height, setHeight] = useState(0);
  const contentRef = useRef<HTMLDivElement>(null);

  const handelClick = () => {
    setHeight((oldHeight) => {
      return oldHeight === 0 && contentRef.current ? contentRef.current.scrollHeight : 0;
    });
  };

  return (
    <li className={classnames(styles.item, styled && styled)}>
      <Button
        type="button"
        styled={classnames(styles.toggle, Boolean(height) && styles.toggle_active)}
        onClick={handelClick}
        >
        {title}
      </Button>
      <div className={styles.content} style={{height: `${height}px`}}>
        <div ref={contentRef}>{children}</div>
      </div>
    </li>
  );
};
