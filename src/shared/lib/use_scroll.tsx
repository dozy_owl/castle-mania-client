import {useEffect, useState} from 'react';


export const useScroll = () => {
  const [scrolling, setScrolling] = useState(false);

  useEffect(() => {
    const scrolledMoreThanThreshold = (scrollYPos: number) => scrollYPos !== 0;

    const onScroll = () => {
      setScrolling(scrolledMoreThanThreshold(window.scrollY));
    };

    window.addEventListener('scroll', onScroll);

    return () => window.removeEventListener('scroll', onScroll);
  }, []);

  return {scrolling};
};
