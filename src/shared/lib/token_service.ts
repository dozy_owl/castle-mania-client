const getSessionToken = () => {
  return sessionStorage.getItem('sessionToken');
};

const createSession = (tk: string) => {
  sessionStorage.setItem('sessionToken', tk);
};

const validateSession = () => {
  return Boolean(sessionStorage.getItem('sessionToken'));
};

const revokeSession = () => {
  sessionStorage.removeItem('sessionToken');
};

export const tokenService = {
  getSessionToken,
  createSession,
  validateSession,
  revokeSession,
};
