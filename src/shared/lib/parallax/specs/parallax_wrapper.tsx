import {FC, ReactNode, useEffect, useRef} from 'react';

import styles from '../index.module.scss';


type ParallaxWrapperProps = {
  children: ReactNode;
};

export const ParallaxWrapper: FC<ParallaxWrapperProps> = ({children}) => {
  const wrapperRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    wrapperRef.current?.addEventListener('mousemove', ({clientX, clientY}) => {
      wrapperRef.current?.style.setProperty('--x', String(clientX));
      wrapperRef.current?.style.setProperty('--y', String(clientY));
    });
  }, []);

  return (
    <div ref={wrapperRef} className={styles.wrapper}>
      {children}
    </div>
  );
};
