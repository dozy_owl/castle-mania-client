import {FC, ReactNode} from 'react';

import styles from '../index.module.scss';


type ParallaxElementProps = {
  children: ReactNode;
};

export const ParallaxElement: FC<ParallaxElementProps> = ({children}) => {
  return <div className={styles.element}>{children}</div>;
};
