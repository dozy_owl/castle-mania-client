import {FC, ReactNode} from 'react';
import {ParallaxElement, ParallaxWrapper} from './specs';


type ParallaxExtensions = {
  Element: typeof ParallaxElement;
  Wrapper: typeof ParallaxWrapper;
};

type ParallaxProps = {children: ReactNode};

export const Parallax: FC<ParallaxProps> & ParallaxExtensions = ({children}) => {
  return <>{children}</>;
};

Parallax.Element = ParallaxElement;
Parallax.Wrapper = ParallaxWrapper;
