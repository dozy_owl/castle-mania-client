import '@testing-library/jest-dom';
import {render as reactRender} from '@testing-library/react';
import {BrowserRouter as Router} from 'react-router-dom';

import App from '../../app/index';


export function renderApp() {
  return reactRender(<App />);
}

export function renderFragment(component: React.ComponentElement<any, any>) {
  return reactRender(<Router>{component}</Router>);
}
