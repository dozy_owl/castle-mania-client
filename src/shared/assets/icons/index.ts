import {ReactComponent as CourseIcon} from './course-icon.svg';
import {ReactComponent as HomeIcon} from './home-icon.svg';
import {ReactComponent as CoursesIcon} from './icon_course_map.svg';
import {ReactComponent as RatingsIcon} from './ratings-icon.svg';


export {HomeIcon, CoursesIcon, CourseIcon, RatingsIcon};
