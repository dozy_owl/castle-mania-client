import {createElement} from 'react';
import {CoursesIcon, HomeIcon} from 'shared/assets/icons';
import {NavItem, NavItemRoute} from '../types';


export const navItemIconMap = {
  [NavItemRoute.MAIN]: createElement(HomeIcon),
  [NavItemRoute.COURSE]: createElement(CoursesIcon),
};

export const navItemNameMap = {
  [NavItemRoute.MAIN]: NavItem.MAIN,
  [NavItemRoute.COURSE]: NavItem.COURSE,
};

export const navItems: NavItemRoute[] = Object.values(NavItemRoute);

export const getNavItemProps = (url: NavItemRoute) => {
  return {
    name: navItemNameMap[url],
    url,
    icon: navItemIconMap[url],
    active: url === window.location.hash,
  };
};
