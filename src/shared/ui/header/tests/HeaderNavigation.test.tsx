import '@testing-library/jest-dom';
import {screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {renderFragment, renderApp} from 'shared/tests/render';

import {Header} from '..';


describe('<HeaderNavigation>', () => {
  it('Дефолтная вариация рендерится в виджете <Header/>', async () => {
    const {container} = renderFragment(<Header />);

    expect(container.querySelector('nav')).toBeInTheDocument();
  });

  it('Содержит ссылку на главную страницу, которой соответствует роут /', async () => {
    renderApp();

    expect(await screen.findByText(/главная/i, {selector: 'a'})).toBeInTheDocument();

    userEvent.click(screen.getByText(/главная/i, {selector: 'a'}));
    expect(global.window.location.pathname).toContain('/');
  });

  it('Содержит ссылку на страницу рейтингов, которой соответствует роут /ratings', async () => {
    renderApp();

    expect(await screen.findByText(/рейтинги/i, {selector: 'a'})).toBeInTheDocument();

    userEvent.click(screen.getByText(/рейтинги/i, {selector: 'a'}));
    expect(global.window.location.pathname).toContain('/ratings');
  });

  it('Содержит ссылку на страницу гильдии, которой соответствует роут /guild', async () => {
    renderApp();

    expect(await screen.findByText(/гильдия/i, {selector: 'a'})).toBeInTheDocument();

    userEvent.click(screen.getByText(/гильдия/i, {selector: 'a'}));
    expect(global.window.location.pathname).toContain('/guild');
  });
});
