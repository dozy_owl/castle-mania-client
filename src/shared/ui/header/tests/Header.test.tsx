import '@testing-library/jest-dom';

import {renderApp} from 'shared/tests/render';


describe('<Header>', () => {
  it('Рендерится в общем лэйауте', async () => {
    const {container} = renderApp();

    expect(container.getElementsByClassName('header')).toBeTruthy();
  });

  it('Содержит блок навигации и виджет <Profile/>', async () => {
    const {container} = renderApp();

    expect(container.getElementsByClassName('header_navigation')).toBeTruthy();
    expect(container.getElementsByClassName('profile')).toBeTruthy();
  });
});
