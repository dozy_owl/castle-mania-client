export enum NavItem {
  MAIN = 'Главная',
  COURSE = 'Карта курса',
}

export enum NavItemRoute {
  MAIN = '#/',
  COURSE = '#/course/1',
}
