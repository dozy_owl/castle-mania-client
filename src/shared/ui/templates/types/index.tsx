import {ReactNode} from 'react';


export type TemplateCommonTypes = {
  styled?: string;
  children?: ReactNode;
};
