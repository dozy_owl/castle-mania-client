import {FC} from 'react';
import styles from '../index.module.scss';
import {TemplateCommonTypes} from '../types';


export const FlexCentererd: FC<TemplateCommonTypes> = ({children, styled = ''}) => {
  return <section className={`${styles.centered} ${styled}`}>{children}</section>;
};
