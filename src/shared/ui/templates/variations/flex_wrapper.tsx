import {FC} from 'react';
import styles from '../index.module.scss';
import {TemplateCommonTypes} from '../types';


export const FlexWrapper: FC<TemplateCommonTypes> = ({children, styled = ''}) => {
  return <div className={`${styles.wrapper} ${styled}`}>{children}</div>;
};
