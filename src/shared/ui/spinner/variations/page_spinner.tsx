import {Template} from 'shared/ui/templates';
import styles from '../index.module.scss';


const PageSpinner = () => {
  return <Template.FlexWrapper styled={styles.page}>Загрузка...</Template.FlexWrapper>;
};

export {PageSpinner};
