import {ReactNode} from 'react';


export type HelmetCommonTypes = {
  heading: ReactNode;
};
