import classnames from 'classnames';
import {FC} from 'react';

import {CommonHeadingProps} from '../types';


export const MainHeading: FC<CommonHeadingProps> = ({heading, styled}) => {
  return <h1 className={classnames(styled)}>{heading}</h1>;
};
