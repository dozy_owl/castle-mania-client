import classnames from 'classnames';
import {FC} from 'react';

import {CommonHeadingProps} from '../types';


export const SectionHeading: FC<CommonHeadingProps> = ({heading, styled}) => {
  return <h2 className={classnames(styled)}>{heading}</h2>;
};
