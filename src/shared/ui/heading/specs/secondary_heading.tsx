import classnames from 'classnames';
import {FC} from 'react';

import {CommonHeadingProps} from '../types';


export const SecondaryHeading: FC<CommonHeadingProps> = ({heading, styled}) => {
  return <h3 className={classnames(styled)}>{heading}</h3>;
};
