import {ReactNode} from 'react';
import {CommonStyledProps} from 'shared/config/types';


export type CommonHeadingProps = {
  heading: ReactNode;
} & CommonStyledProps;
