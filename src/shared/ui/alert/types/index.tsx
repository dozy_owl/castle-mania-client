import {ReactNode} from 'react';


export type CommonAlertProps = {
  onMouseEnter: () => void;
  onMouseLeave: () => void;
  children: ReactNode;
};
