import {FC, ReactNode} from 'react';

import styles from '../index.module.scss';


type AlertMessageProps = {
  message: ReactNode;
};

export const AlertMessage: FC<AlertMessageProps> = ({message}) => {
  return <span className={styles.message}>{message}</span>;
};
