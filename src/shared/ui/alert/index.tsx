import {FC, ReactNode} from 'react';

import {CommonStyledProps} from 'shared/config/types';
import styles from './index.module.scss';
import {AlertMessage} from './specs';
import {CommonAlertProps} from './types';


type AlertExtensions = {
  Message: typeof AlertMessage;
};

type AlertProps = CommonStyledProps &
  CommonAlertProps & {
    children: ReactNode;
  };

export const Alert: FC<AlertProps> & AlertExtensions = ({children, ...props}) => {
  return (
    <div className={styles.base} {...props}>
      {children}
    </div>
  );
};

Alert.Message = AlertMessage;
