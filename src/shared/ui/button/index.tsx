import {FC, ReactNode, ButtonHTMLAttributes} from 'react';
import {CommonStyledProps} from 'shared/config/types';

import styles from './index.module.scss';


type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & 
  CommonStyledProps & {
    onClick?: (e?: MouseEvent | Event) => void;
    children?: ReactNode;
  };

export const Button: FC<ButtonProps> = ({
  type,
  children,
  styled = '',
  onClick,
  ...rest
}) => {
  return (
    <button type={type}
            className={`${styles.button} ${styled}`}
            onClick={onClick}
            {...rest}>{children}</button>
  );
};

Button.defaultProps = {
  onClick: () => null,
  type: 'submit',
  children: null,
};
