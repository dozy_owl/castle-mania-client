import classnames from 'classnames';
import {FC, ReactNode} from 'react';

import styles from './index.module.scss';
import {CommonBannerProps} from './types';


type BannerProps = CommonBannerProps & {
  children: ReactNode;
};

export const Banner: FC<BannerProps> = ({children, styled}) => {
  return (
    <section className={classnames(styles.base, Boolean(styled) && styled)}>
      {children}
    </section>
  );
};
