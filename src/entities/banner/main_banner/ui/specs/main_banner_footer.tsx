import {FC, ReactNode} from 'react';
import {Template} from 'shared/ui';

import styles from '../index.module.scss';


type MainBannerFooterProps = {
  children: ReactNode;
};

export const MainBannerFooter: FC<MainBannerFooterProps> = ({children}) => {
  return <Template.FlexWrapper styled={styles.footer}>{children}</Template.FlexWrapper>;
};
