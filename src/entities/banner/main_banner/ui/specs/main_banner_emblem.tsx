import {Heading, Template} from 'shared/ui';
import styles from '../index.module.scss';


export const MainBannerEmblem = () => {
  return (
    <Template.FlexWrapper styled={styles.emblem}>
      <span className={styles.emblem_shield} />
      <Heading.Main heading="Образование по новому" 
                    styled={styles.emblem_title} />
      <span className={styles.emblem_pennant__front} />
      <span className={styles.emblem_pennant__back} />
    </Template.FlexWrapper>
  );
};
