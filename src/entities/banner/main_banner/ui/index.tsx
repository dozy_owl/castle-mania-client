import {FC, ReactNode} from 'react';
import {Parallax} from 'shared/lib/parallax';
import {Banner, Template} from 'shared/ui';
import styles from './index.module.scss';
import {MainBannerEmblem, MainBannerFooter} from './specs';


type UIMainBannerExtensions = {
  Emblem: typeof MainBannerEmblem;
  Footer: typeof MainBannerFooter;
};

type UIMainBannerProps = {children: ReactNode};

export const UIMainBanner: FC<UIMainBannerProps> & UIMainBannerExtensions = ({
  children,
}) => {
  return (
    <Parallax.Wrapper>
      <Banner styled={styles.base}>
        <span className={styles.background}/>
        <Template.FlexContainer styled={styles.footer}>
          {children}
        </Template.FlexContainer>
      </Banner>
    </Parallax.Wrapper>
  );
};

UIMainBanner.Emblem = MainBannerEmblem;
UIMainBanner.Footer = MainBannerFooter;
