import {FC} from 'react';
import {Link, useParams} from 'react-router-dom';
import {v4 as uuid} from 'uuid';

import {Accordion} from 'shared/lib/accordion';
import {useCourse, useCourseWeek} from 'shared/lib/store';
import styles from '../index.module.scss';


type CourseInfoChaptersProps = {
  chapters: any[];
};

export const CourseInfoChapters: FC<CourseInfoChaptersProps> = ({chapters}) => {
  const setWeek = useCourseWeek((state) => state.setWeek);
  const {courseId} = useParams();

  return (
    <Accordion styled={styles.links}>
      {chapters.map((chapter) => (
        <Link key={uuid()}
              className={styles.link}
              to={`/course/${courseId}/material/${chapter.id}`}
              onClick={() => setWeek(chapter.id)}>{chapter.title}</Link>
      ))}
    </Accordion>
  );
};
