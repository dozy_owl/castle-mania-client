import {FC, ReactNode} from 'react';

import {Template} from 'shared/ui';
import styles from './index.module.scss';
import {CourseInfoChapters} from './specs';


type CourseInfoExtensions = {
  Chapters: typeof CourseInfoChapters;
};

type UICourseInfoProps = {title: string; score: number; children: ReactNode};

export const UICourseInfo: FC<UICourseInfoProps> & CourseInfoExtensions = ({
  title,
  score,
  children,
}) => {
  return (
    <Template.FlexWrapper styled={styles.base}>
      <div className={styles.header}>
        <h3>{title}</h3>
        <span className={styles.score}>{score}</span>
      </div>
      {children}
    </Template.FlexWrapper>
  );
};

UICourseInfo.Chapters = CourseInfoChapters;
