import {FC, ReactNode} from 'react';
import {useNavigate} from 'react-router-dom';
import {Button, Template} from 'shared/ui';
import styles from './index.module.scss';
import {CourseCardProgress, CourseCardDescription} from './specs';


type CourseCardExtensions = {
  Progress: typeof CourseCardProgress;
  Description: typeof CourseCardDescription;
};

type CourseCardProps = {
  children: ReactNode;
};

export const UICourseCard: FC<CourseCardProps> & CourseCardExtensions = ({children}) => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/course/1');
  };

  return (
    <Template.FlexWrapper styled={styles.base}>
      {children}
      <Button type="button" onClick={handleClick} styled={styles.enter_course}>
        Приступить
      </Button>
    </Template.FlexWrapper>
  );
};

UICourseCard.Progress = CourseCardProgress;
UICourseCard.Description = CourseCardDescription;
