import {FC, memo} from 'react';
import {Badge, Template} from 'shared/ui';

import styles from '../index.module.scss';


type CourseCardProgressProps = {
  theoryCompleted?: boolean;
  theoryProgress?: string;
  testsProgress?: string;
};

const badgeProps = {
  statusActive: 'Активно',
  statusCompleted: 'Пройдено',
  statusForbidden: (
    <>
      Недоступно, <br />
      пока не пройдено обучение
    </>
  ),

  titleTheory: 'Теория',
  titleTests: 'Практика',
};

export const CourseCardProgress: FC<CourseCardProgressProps> = ({
  theoryCompleted,
  theoryProgress,
  testsProgress,
}) => {
  return (
    <Template.FlexWrapper styled={styles.progress}>
      {theoryCompleted ? (
        <>
          <Badge title={badgeProps.titleTheory} notification={badgeProps.statusCompleted}>
            <Badge.Allowed info={theoryProgress} withEffect />
          </Badge>
          <Badge title={badgeProps.titleTests} notification={badgeProps.statusActive}>
            <Badge.Allowed info={testsProgress} withEffect />
          </Badge>
        </>
      ) : (
        <>
          <Badge title={badgeProps.titleTheory} notification={badgeProps.statusActive}>
            <Badge.Allowed info={theoryProgress} withEffect />
          </Badge>
          <Badge title={badgeProps.titleTests} notification={badgeProps.statusForbidden}>
            <Badge.Forbidden withEffect />
          </Badge>
        </>
      )}
    </Template.FlexWrapper>
  );
};

CourseCardProgress.defaultProps = {
  theoryCompleted: false,
  theoryProgress: '0',
  testsProgress: '0',
};

export default memo(CourseCardProgress);
