import {FC, memo, ReactNode} from 'react';
import {Heading, Template} from 'shared/ui';

import styles from '../index.module.scss';


type CourseCardDescriptionProps = {
  name: ReactNode;
  description: ReactNode;
};

export const CourseCardDescription: FC<CourseCardDescriptionProps> = ({
  name,
  description,
}) => {
  return (
    <Template.FlexWrapper styled={styles.description}>
      <Heading.Secondary heading={name}
                         styled={styles.title} />
      <span>{description}</span>
    </Template.FlexWrapper>
  );
};

export default memo(CourseCardDescription);
