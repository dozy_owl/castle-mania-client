import {ReactNode} from 'react';


export type CourseCardCommonTypes = {
  title: ReactNode;
};
