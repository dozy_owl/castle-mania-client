import {useMutation} from 'react-query';
import {useCourseMaterials} from 'shared/lib/store';


type AsyncQuery = (params: any) => Promise<any>;

export const useAnswerQuestionMutation = (query: AsyncQuery) => {
  const {
    activeMaterial, 
    setActiveMaterialPassed, 
    setActiveMaterialUnpassed
  } = useCourseMaterials();

  return useMutation(
    async (params: any) => {
      const points = await query({
        questionId: activeMaterial?.id, 
        variantIds: [params.singleAnswer], 
        openAnswer: params.openAnswer, 
      });
      if (points === 0) {
        throw new Error('Неверный ответ');
      }
    }, {
      onSuccess() {
        if (activeMaterial) {
          setActiveMaterialPassed(activeMaterial.id);
        }
      },
      onError() {
        if (activeMaterial) {
          setActiveMaterialUnpassed(activeMaterial.id);
        }
      },
    });
};
