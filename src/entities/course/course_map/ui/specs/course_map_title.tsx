import classnames from 'classnames';

import {useCourse} from 'shared/lib/store';
import styles from '../index.module.scss';


export const CourseMapTitle = () => {
  const {name, experience} = useCourse((state) => state);

  return (
    <div className={classnames(styles.course_title)}>
      <h1>{name}</h1>
      <span className={styles.course_title__score}>{experience}</span>
    </div>
  );
};
