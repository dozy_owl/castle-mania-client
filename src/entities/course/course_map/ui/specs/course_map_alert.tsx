import {FC, ReactNode} from 'react';

import styles from '../index.module.scss';


type CourseMapAlertProps = {
  title: string;
  children: ReactNode;
};

export const CourseMapAlert: FC<CourseMapAlertProps> = ({
  title,
  children,
}) => {
  return (
    <div className={styles.alert}>
      <span className={styles.alert_title}>{title}</span>
      {children}
    </div>
  );
};
