import classnames from 'classnames';
import {FC, ReactNode} from 'react';

import styles from '../index.module.scss';


type CourseMapTileProps = {
  order: number;
  cycled: boolean;
  children: ReactNode;
};

export const CourseMapTile: FC<CourseMapTileProps> = ({order, cycled, children}) => {
  return (
    <span className={
      classnames(
        styles.tile, 
        styles[`tile_${Number(cycled)}${order % 8}`])}>{children}</span>
  );
};
