import classnames from 'classnames';
import {FC} from 'react';

import {useCourse, useCourseWeek} from 'shared/lib/store';
import {Button} from 'shared/ui';
import styles from '../index.module.scss';


type CourseMapChapterProps = {
  id: string;
  order: number;
  cycled: boolean;
  progress: number;
};

export const CourseMapChapter: FC<CourseMapChapterProps> = ({
  id,
  order,
  cycled,
  progress,
}) => {
  const {chapters, activeChapter, setActiveChapter} = useCourse();
  const setWeek = useCourseWeek((state) => state.setWeek);

  const active = activeChapter?.id === id;
  const disabled = order > 1;

  const handlePickChapter = (id: string) => {
    const index = chapters.findIndex(
      (chapter) => chapter.id === id);
    if (index !== -1) {
      const newActiveChapter = 
          chapters[index];
      setActiveChapter(newActiveChapter.id);
      setWeek(newActiveChapter);
    }
  };

  return (
    <Button disabled={order > 1}
            styled={classnames(
              styles.checkpoint,
              active && styles.checkpoint__active,
              progress && styles.checkpoint__in_progress,
              disabled && styles.checkpoint__disabled,
              styles[`checkpoint_${Number(cycled)}${order % 8}`])}
            onClick={() => handlePickChapter(id)}>
      {!disabled && <span className={styles.checkpoint__progress}>{progress}</span>}
    </Button>
  );
};
