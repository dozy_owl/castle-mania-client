import classnames from 'classnames';
import {FC, ReactNode, useRef} from 'react';

import styles from './index.module.scss';
import {CourseMapAlert, CourseMapTile, CourseMapTitle, CourseMapChapter} from './specs';


type UICourseMapExtensions = {
  Alert: typeof CourseMapAlert;
  Chapter: typeof CourseMapChapter;
  Title: typeof CourseMapTitle;
  Tile: typeof CourseMapTile;
};

type UICourseMapProps = {
  children: ReactNode;
};

export const UICourseMap: FC<UICourseMapProps> & UICourseMapExtensions = ({children}) => {
  const mapRef = useRef<HTMLDivElement>(null);

  return (
    <div ref={mapRef} className={styles.base}>
      <span className={classnames(styles.tile, styles.tile_start)}/>
      {children}
      <span className={classnames(styles.tile, styles.tile_end)}/>
    </div>
  );
};

UICourseMap.Alert = CourseMapAlert;
UICourseMap.Chapter = CourseMapChapter;
UICourseMap.Title = CourseMapTitle;
UICourseMap.Tile = CourseMapTile;
