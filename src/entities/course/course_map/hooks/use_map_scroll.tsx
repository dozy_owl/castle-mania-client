import {useCallback, useState} from 'react';


export const useMapScroll = (userScroll: number) => {
  const [scrollX, setScrollX] = useState(0);

  const isScrollAllowed = useCallback(
    (mapWrapper: HTMLDivElement, direction: string, maxScroll: number) => {
      return direction === 'LEFT'
        ? -scrollX
            < mapWrapper.clientWidth - window.innerWidth + window.innerWidth * 0.1 + 300
        : scrollX + userScroll <= maxScroll;
    },
    [userScroll, scrollX],
  );

  const onScroll = (
    mapWrapper: HTMLDivElement | null,
    direction: string,
    maxScroll: number,
  ) => {
    if (!mapWrapper || !direction) {
      return;
    }

    if (!isScrollAllowed(mapWrapper, direction, maxScroll)) {
      return;
    }

    if (direction === 'RIGHT') {
      mapWrapper.style.setProperty('--map-shift', `${scrollX + userScroll}`);
      setScrollX((old) => old + userScroll);
    } else {
      mapWrapper.style.setProperty('--map-shift', `${scrollX - userScroll}`);
      setScrollX((old) => old - userScroll);
    }
  };

  return {onScroll};
};
